/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-08-05
 */
public class Fact {
    private Fact() {
    }

    static public double parse(Tokenizer tokenizer){
        double v;
        if (tokenizer.peek() == '('){
            tokenizer.readToken();
            v = Expr.parseExpr(tokenizer);

            char c = tokenizer.readToken();

            if (c != ')'){
                System.out.println("syntax error \"" + c + "\" should be \")\"");
                v = Double.NaN;
            }
        } else {
            v = Num.parse(tokenizer);
        }

        return v;
    }
}
