/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-08-05
 */
public enum Op1 {
    mult,
    div,
    err;

    public static Op1 parse(Tokenizer tokenizer){
        Op1 ret;

        if (tokenizer.isEmpty())
            return err;

        if (tokenizer.peek() == '*'){
            tokenizer.readToken();
            ret = mult;
        } else if (tokenizer.peek() == '/'){
            tokenizer.readToken();
            ret = div;
        } else {
            ret = err;
        }

        return ret;
    }
}
