/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-08-05
 */
public class Expr {
    static public double parseExpr(Tokenizer tokenizer){
        double v = Term.parse(tokenizer);

        Op2 operator;

        while (!(operator = Op2.parse(tokenizer)).equals(Op2.err)){
            double nextValue = Term.parse(tokenizer);

            if (operator.equals(Op2.addition)){
                v += nextValue;
            } else if (operator.equals(Op2.subratction)){
                v -= nextValue;
            }
        }

        return v;
    }
}
