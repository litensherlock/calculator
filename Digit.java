/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-08-05
 */
public class Digit {
    private static char[] digits = {'0','1','2','3','4','5','6','7','8','9'};

    private Digit() {
    }

    public static Character parse(Tokenizer tokenizer){
        if (tokenizer.isEmpty())
            return null;

        char digit = tokenizer.peek();
        if (isDigit(digit)){
            tokenizer.readToken();
            return digit;
        } else {
            return null;
        }
    }

    private static boolean isDigit(char c){
        for (char digit : digits) {
            if (digit == c){
                return true;
            }
        }

        return false;
    }
}
