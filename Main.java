import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-08-05
 */
public class Main {

    public static void main(String[] args) {
        String test = "1+2/4-3*(3--2)";

        Scanner sc = new Scanner(System.in);

        Parser parser = new Parser();

        System.out.println("Write an math expression with the basic math operators. \n\rexample (2+3*2)/2 \n\rwrite exit and press return to exit");

        while (sc.hasNext()){

            String s = sc.nextLine();

            if (s.equals("exit"))
                return;

            double v = parser.parse(s);

            System.out.println("=" + v);
        }
    }
}
