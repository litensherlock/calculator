/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-08-05
 */
public class Tokenizer {
    private int idx = 0;
    private String string;

    public Tokenizer(String string) {
        this.string = string;
    }

    public char peek(){
        return string.charAt(idx);
    }

    public char readToken() {
        return string.charAt(idx++);
    }

    public boolean isEmpty(){
        return idx >= string.length();
    }

    public int getIdx(){return idx;}
}
