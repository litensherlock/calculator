/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-08-05
 */
public class Term {
    static public double parse(Tokenizer tokenizer){
        double v = Fact.parse(tokenizer);

        Op1 operator;

        while (!(operator = Op1.parse(tokenizer)).equals(Op1.err)){
            double nextValue = Fact.parse(tokenizer);

            if (operator.equals(Op1.mult)){
                v *= nextValue;

            } else if (operator.equals(Op1.div)){
                v /= nextValue;
            }
        }

        return v;
    }
}
