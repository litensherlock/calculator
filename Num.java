/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-08-05
 */
public class Num {
    public static double parse(Tokenizer tokenizer){
        boolean neg = false;

        if (tokenizer.peek() == '-'){
            tokenizer.readToken();
            neg = true;
        }

        StringBuilder sb = new StringBuilder();

        Character c;

        while ((c = Digit.parse(tokenizer)) != null){
            sb.append(c);
        }

        double v = Double.parseDouble(sb.toString());

        return neg ? -v : v;
    }
}
