/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-08-05
 */
public enum Op2 {
    addition,
    subratction,
    err;

    public static Op2 parse(Tokenizer tokenizer){
        Op2 ret;

        if (tokenizer.isEmpty())
            return err;

        if (tokenizer.peek() == '+'){
            tokenizer.readToken();
            ret = addition;
        } else if (tokenizer.peek() == '-'){
            tokenizer.readToken();
            ret = subratction;
        } else {
            ret = err;
        }

        return ret;
    }
}
