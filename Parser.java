
/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2014-08-05
 */
public class Parser {

    public double parse(String s){
        Tokenizer st = new Tokenizer(s);

        return Expr.parseExpr(st);
    }


}
